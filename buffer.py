#!/usr/bin/env python

import copy


class BufferError(Exception):
    pass


class State(object):
    def __init__(self, offset=0, unread=True):
        self.offset = offset
        self.unread = unread
        self.eof = False


class Buffer(object):
    def __init__(self, text):
        self.text = text
        self.state = State()

    def read(self, nchars):
        if nchars < 1:
            Exception("Bad nchars value.")

        if self.state.eof:
            raise Exception('Attempt to read buffer beyond EOF.')

        text = self.text[self.state.offset:self.state.offset+nchars]
        self.state.offset += len(text)

        if self.state.offset == len(self.text):
            self.state.eof = True

        return text

    def get_state(self):
        return copy.copy(self.state)

    def set_state(self, state):
        self.state = state


if __name__ == "__main__":
    unittest.main()
