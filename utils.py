import re


def _repr(obj):
    if isinstance(obj, set):
        return "<{}>".format(" ".join(map(str, obj)))


def _cut(text):
    l = 24

    if len(text) > l:
        t = u"{}...".format(text[:l])
    else:
        t = text

    return re.compile("\n").sub(u"\\\\n", t)


def _rule_at(rule_at):
    return "{}{}".format(*rule_at) if rule_at is not None else repr(None)
