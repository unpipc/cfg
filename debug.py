Debug = 0

Margin = [lambda: '']

def set_margin_func(func):
    Margin[0] = func

def debug(msg, level=0):
    if level < Debug:
        print "%s%s" % (Margin[0](), msg.encode("utf-8"))
