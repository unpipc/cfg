#!/usr/bin/env python

class Error(object):
    def __init__(self, message, offset):
        self.message = message
        self.offset = offset

    def __str__(self):
        return "Error at position %d.\n%s" % (self.offset, self.message)


class EUnexpected(Error):
    def __init__(self, expected_symbols, offset):
        super(EUnexpected, self).__init__("Expected: %s" % ", ".join(expected_symbols), offset)


class ESyntax(Error):
    def __init__(self, offset):
        super(ESyntax, self).__init__("Syntax error", offset)


