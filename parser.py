#!/usr/bin/env python

from bnf import *
import unittest


class Parser(object):
    def __init__(self, bnf, buf):
        self.bnf = bnf
        self.buf = buf

    def parse(self, target):
        return self.bnf.get_rule(target).parse(self.buf)

class Test(unittest.TestCase):
    def print_result(self, result):
        if result[0]:
            et.dump(result[1])
        else:
            print "Syntax error at %d" % result[1]

    def test_parser(self):
        # FIXME
        return
        bnf = BNF()
        bnf.define('A', ['(', SYM('A'), ')'])
        bnf.define('A', LAMBDA)
        bnf.define('T', [SYM('A'), EOF])

        p = Parser(bnf, Buffer('()'))
        self.print_result(p.parse('T'))

    def test_mutual_references(self):
        bnf = BNF()
        bnf.define('A', SYM('B'))
        bnf.define('A', 'a')
        bnf.define('B', SYM('A'))

        p = Parser(bnf, Buffer('a'))

        self.print_result(p.parse('A'))

if __name__ == "__main__":
    unittest.main()
