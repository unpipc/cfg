#!/usr/bin/env python

import unittest


class Cache(object):
    def __init__(self):
        self.nodes = dict()
        self.errors = dict()

    def get_node(self, rule, text_span, align):
        suitable = lambda node: bool(node.span[1] <= text_span[1])
        perfect = lambda node: bool(node.span[1] == text_span[1])

        key = (text_span[0], rule)
        nodes = self.nodes.get(key)

        if nodes:
            candidates = sorted(filter(suitable, nodes), lambda a, b: cmp(b.span[1], a.span[1]))

            if candidates and (align == 'left' or perfect(candidates[0])):
                return candidates[0]

    def set_node(self, node, rule):
        key = (node.span[0], rule)

        if key not in self.nodes:
            self.nodes[key] = set()

        self.nodes[key] |= {node}

    def get_left_error(self, rule, text_span):
        span, e = self.errors.get((rule, text_span[0], 'left'),
                                  (None, None))

        if span and text_span[1] <= span[1]:
            return e

    def get_fit_error(self, rule, text_span):
        errors = filter(lambda (span, e): span[1] == text_span[1],
                        self.errors.get((rule, text_span[0], 'fit'), set()))

        assert len(errors) in (0, 1)

        if errors:
            _, e = errors[0]
            return e

    def set_left_error(self, error, rule, text_span):
        key = (rule, text_span[0], 'left')
        span, _ = self.errors.get(key, (None, None))

        if not span or span[1] < text_span[1]:
            self.errors[key] = (text_span, error)

            superseded_fit_errors = filter(lambda (span, _e): span[1] <= text_span[1],
                                           self.errors.get((rule, text_span[0], 'fit'),
                                                           set()))

            if superseded_fit_errors:
                fit_key = (rule, text_span[0], 'fit')

                self.errors[fit_key] -= set(superseded_fit_errors)

                if not self.errors[fit_key]:
                    del self.errors[fit_key]

    def set_fit_error(self, error, rule, text_span):
        if not self.get_left_error(rule, text_span):
            key = (rule, text_span[0], 'fit')

            if key not in self.errors:
                self.errors[key] = set()

            self.errors[key] |= {(text_span, error)}

    def get_error(self, rule, text_span, align):
        e = self.get_left_error(rule, text_span)

        if not e and align == 'fit':
             e = self.get_fit_error(rule, text_span)

        return e

    def set_error(self, error, rule, text_span, align):
        if align == 'left':
            f = self.set_left_error
        else:
            f = self.set_fit_error

        return f(error, rule, text_span)

    def get(self, rule, text_span, align):
        node = self.get_node(rule, text_span, align)

        if node:
            return node

        return self.get_error(rule, text_span, align)

    def get_nodes_data(self):
        return "\n".join(map(lambda ((at, rule), nodes): "@%d: %s, %s" % (at, rule.symbol, nodes),
                             self.cache.nodes.iteritems()))


class Test(unittest.TestCase):
    def test_node(self):
        c = Cache()
        rule = String(None, 'abc')
        node = TextNode('A', 'abc', 0)

        c.set_node(node, rule)

        self.assertEqual(c.get_node(rule, (0, 1000), 'left'), node)
        self.assertEqual(c.get_node(rule, (0, 3), 'left'), node)

        self.assertEqual(c.get_node(rule, (1, 3), 'left'), None)
        self.assertEqual(c.get_node(rule, (0, 2), 'left'), None)

    def test_error(self):
        c = Cache()
        rule = String(None, 'abc')
        e = ParsingError(0, rule=rule)

        c.set_error(e, rule, (0, 4), 'left')
        self.assertEqual(c.get_error(rule, (0, 4), 'left'), e)
        self.assertEqual(c.get_error(rule, (0, 4), 'fit'), e)

        self.assertEqual(c.get_error(rule, (1, 4), 'left'), None)
        self.assertEqual(c.get_error(rule, (1, 4), 'fit'), None)
        self.assertEqual(c.get_error(rule, (0, 5), 'left'), None)
        self.assertEqual(c.get_error(rule, (0, 5), 'fit'), None)

        c.set_error(e, rule, (0, 10), 'fit')
        self.assertEqual(c.get_error(rule, (0, 10), 'left'), None)
        self.assertEqual(c.get_error(rule, (0, 10), 'fit'), e)

        self.assertEqual(c.get_error(rule, (0, 7), 'left'), None)
        self.assertEqual(c.get_error(rule, (0, 7), 'fit'), None)

        c.set_error(e, rule, (0, 100), 'left')
        print c.errors
        self.assertEqual(len(c.errors), 1)

if __name__ == "__main__":
    from rules import String
    from nodes import TextNode
    from errors import ParsingError

    unittest.main()
