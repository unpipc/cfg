#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Правило скобок:
S: '(', S, ')'
S: '[', S, ']'
S: '{', S, '}'
S: ''

Вызов функции python с переменными в качестве аргументов:
NAME: [a-zA-Z0-9_]{1,256}
C: NAME '(' ARGS ')'
LIST: NAME
LIST: LIST ',' NAME
ARGS: ''
ARGS: LIST
"""

import re, abc, unittest
from nodes import *
from processor import *
from errors import *
from bnf import BNF
from debug import Debug, debug
from utils import _cut, _repr

class UnknownSymbol(Exception):
    def __init__(self, symbol):
        super(UnknownSymbol, self).__init__("Unknown symbol: %s" % symbol)


class Product(object):
    def __init__(self, **kwargs):
        self.terminal_level = kwargs.get('terminal_level', 0)
        self.looped = kwargs.get('looped', False)
        self.repeat = kwargs.get('repeat', False)

    def greedy_key(self):
        #return (not self.looped), (self.terminal_level is None, self.terminal_level)
        return (not self.repeat), (not self.looped), self.terminal_level

    def fast_key(self):
        return self.looped, self.terminal_level

    def __cmp__(self, other):
        return -1 if other is None else cmp(self.fast_key(), other.fast_key())

    def __repr__(self):
        looped = "<-" if self.looped else ""
        terminal = "-" if self.terminal_level is None else self.terminal_level

        return 'P%s%s' % (terminal, looped)


class Rule(object):
    __metaclass__ = abc.ABCMeta
    terminal = False

    def __init__(self, bnf, symbol=None):
        self.bnf = bnf
        self.symbol = symbol or self.__class__.__name__
        self.product = None

    def __repr__(self):
        if self.symbol == self.__class__.__name__:
            klass = ''
        else:
            klass = "(%s)" % self.__class__.__name__

        return "Rule %s%s" % (self.symbol, klass)

    def parse(self, *args, **kwargs):
        raise Exception("Undefined method")


class TerminalRule(Rule):
    __metaclass__ = abc.ABCMeta
    terminal = True

    def __init__(self, bnf, symbol=''):
        super(TerminalRule, self).__init__(bnf, symbol)

    def get_rexp(self):
        """
        Get regular expression for the matching terminal symbols.
        """
        raise Exception("Not implemented")

    def resolve(self):
        self.product = Product(terminal_level=0)
        return self.product


class Ref(Rule):
    def __init__(self, bnf, ref_symbol):
        super(Ref, self).__init__(bnf)
        self.ref = ref_symbol
        self.rule = None

    def __repr__(self):
        return "*%s" % self.ref

    def resolve(self):
        self.rule = self.bnf.rules.get(self.ref)

        if not self.rule:
            raise UnknownSymbol(self.ref)

        ref_product = self.bnf.resolve(self.rule)

        if ref_product is not None:
            self.product = Product(terminal_level=ref_product.terminal_level + 1,
                                   repeat=ref_product.repeat)

        return self.product

    def parse(self, *args, **kwargs):
        return self.bnf._parse_rule(self.rule, *args, **kwargs)


class Choice(Rule):
    def __init__(self, bnf, rules, symbol=None):
        super(Choice, self).__init__(bnf, symbol)
        self.rules = list(rules)

        # May have different precedence orders for different align methods
        # in auto-choice mode.
        self.__rules_ordered = {'left': None, 'fit': None}


    def __repr__(self):
        #return "%s{%s}" % (self.symbol, "|".join(r.symbol for r in self.rules))
        return "%s{}" % self.symbol

    def resolve(self):
        products = filter(lambda p: bool(p),
                          map(self.bnf.resolve, self.rules))
        if products:
            self.product = Product(terminal_level=min(map(lambda p: p.terminal_level,
                                                          products)) + 1,
                                   repeat=any(p.repeat for p in products))
        return self.product

    def parse(self, text, offset=0, align='fit'):
        if self.__rules_ordered[align] is None:
            if self.bnf.auto_choice:
                if align == 'left':
                    key = lambda r: r.product.greedy_key()
                else:
                    key = lambda r: r.product.fast_key()

                self.__rules_ordered[align] = sorted(self.rules, key=key)
            else:
                self.__rules_ordered[align] = self.rules

        errors = []
        void_nodes = []

        with_products = ["{}-{}".format(r, r.product) for r in self.__rules_ordered[align]]
        debug("{}{{{}}}".format(self.symbol, ", ".join(with_products)), 1)

        for rule in self.__rules_ordered[align]:
            debug(u"{} try {}:{}".format(self, rule, align), 1)

            try:
                child = self.bnf._parse_rule(rule, text, offset, align)

                if child.is_void():
                    void_nodes.append(child)
                else:
                    return Node(self.symbol, child)
            except LoopingError as e:
                debug('Looping error at choice for rule: %s' % e.rule)
                pass
            except ParsingError as e:
                errors.append(e)

        if void_nodes:
            # Only void nodes found.
            # Prefer to return lambda or empty text node over nodes with children.
            lambda_nodes = filter(lambda n: isinstance(n, LambdaNode), void_nodes)
            text_nodes = filter(lambda n: isinstance(n, TextNode), void_nodes)

            for void_candidates in (lambda_nodes, text_nodes, void_nodes):
                if void_candidates:
                    return Node(self.symbol, void_candidates[0])
        else:
            sorted_errors = sorted(errors, key=self.bnf._error_preference_key())
            debug("Errors at: %s" % ", ".join(map(lambda e: str(e.index), sorted_errors)))

            raise sorted_errors[0]


class Chain(Rule):
    def __init__(self, bnf, rules, symbol=None):
        super(Chain, self).__init__(bnf, symbol)
        self.rules = tuple(rules)

    def parse(self, text, offset=0, align='fit'):
        # TODO: decide when to use parsing with binding.
        # TODO: Basically, we need that when first non-terminal symbol
        # TODO: cannot give anything, but lambda or parsing error.

        if self.product is not None and self.product.looped:
        #if self.rules and \
        #   self.rules[0].product is not None and \
        #   self.rules[0].product.looped:

            child_nodes = parse_w_binding(self.bnf, text, self.rules, offset, align)
        else:
            child_nodes = []
            cursor = 0

            for align, rule in zip(['left'] * (len(self.rules)-1) + [align], self.rules):
                child_nodes.append(self.bnf._parse_rule(rule, text[cursor:], offset+cursor, align))
                cursor = child_nodes[-1].span[1] - offset

        return Node(self.symbol, *child_nodes)

    def resolve(self):
        products = map(self.bnf.resolve, self.rules)

        if None not in products:
            self.product = Product(terminal_level=max(map(lambda p: p.terminal_level,
                                                          products)) + 1,
                                   repeat=any(p.repeat for p in products))
        return self.product

    def __repr__(self):
        #return '[%s]' % ', '.join(map(repr, self.rules))
        return '%s[]' % self.symbol


class Repeat(Rule):
    def __init__(self, bnf, rule, symbol, nmin, nmax):
        '''See class REPEAT() for help on arguments validation.'''
        super(Repeat, self).__init__(bnf, symbol)

        self.rule = rule
        self.nmin = nmin
        self.nmax = nmin if nmax is None else nmax

    def parse(self, text, offset=0, align='fit'):
        at = 0
        nodes = []
        error = None

        need = lambda: len(nodes) < self.nmin
        can_get = lambda: self.nmax == 0 or len(nodes) < self.nmax

        while text[at:] and (need() or can_get()):
            try:
                node = self.bnf._parse_rule(self.rule, text[at:], offset+at, 'left')
                nodes.append(node)

                if node.is_void():
                    # Node is void, no chance to get non-void node on the next iteration.
                    debug("Cached nodes: %s" % self.bnf.cache.nodes, 1)
                    break

                at = node.span[1] - offset
            except ParsingError as e:
                error = e
                break
            except LoopingError as e:
                debug('Looping error at repeat for rule: %s' % e.rule)
                error = e
                break

        if need() or (align == 'fit' and text[at:]):
            raise error if error else ParsingError(at + offset, rule=self.rule)

        return Node(self.symbol, *nodes, offset=offset)

    def resolve(self):
        product = self.bnf.resolve(self.rule)

        if product:
            self.product = Product(terminal_level=product.terminal_level + 1,
                                   repeat=True)
        elif self.nmin == 0:
            # Non-productive, but optional rule.
            # Allow for the sake of consistency (it can parse empty string).
            self.product = Product(terminal_level=0)

        return self.product

    def __repr__(self):
        #return '%s%d,%s}' % (repr(self.rule), self.nmin, '' if self.nmax == 0 else self.nmax)
        return '%s+' % self.symbol

def shared_start(s1, s2):
    nshared = 0

    for i, (a, b) in enumerate(zip(s1, s2), 1):
        if a == b:
            nshared = i
        else:
            break

    return s1[:nshared]


class Lambda(TerminalRule):
    def __init__(self, bnf, symbol=None):
        super(Lambda, self).__init__(bnf, symbol)

    def get_rexp(self):
        return re.compile('')

    def __repr__(self):
        return "Lambda{}".format(self.symbol if self.symbol != "Lambda" else "")

    def parse(self, text, offset=0, align='fit'):
        if align == 'left' or not text:
            return LambdaNode(self.symbol, offset)
        else:
            raise ParsingError(offset, rule=self)


class String(TerminalRule):
    def __init__(self, bnf, string, symbol=None):
        super(String, self).__init__(bnf, symbol)
        self.string = str(string)

    def get_rexp(self):
        return re.compile(re.escape(self.string))

    def parse(self, text, offset=0, align='fit'):
        assert align in {'left', 'fit'}

        status = text.startswith(self.string)

        if status and align == 'fit':
            status = bool(len(text) == len(self.string))

        if status:
            return TextNode(self.symbol, self.string, offset)
        else:
            raise ParsingError(offset + len(shared_start(text, self.string)),
                                   rule=self)

    def __repr__(self):
        return repr(self.string)

class Re(TerminalRule):
    def __init__(self, bnf, re_str, symbol=None):
        super(Re, self).__init__(bnf, symbol)
        self.rexp = re.compile(re_str)

    def get_rexp(self):
        return self.rexp

    def __repr__(self):
        return repr(self.rexp.pattern)

    def parse(self, text, offset=0, align='fit'):
        assert align in {'left', 'fit'}

        error_at = None
        m = self.rexp.search(text)

        if m:
            span = m.span()

            if span[0] == 0:
                if align == 'fit' and len(text) != span[1]:
                    error_at = span[1]
            else:
                error_at = 0
        else:
            error_at = 0

        if error_at is None:
            return TextNode(self.symbol, text[:span[1]], offset)
        else:
            raise ParsingError(offset + error_at,
                                   rule=self)


def presult(result, buf):
    def perror(e, buf):
        print e

        head = 16
        tail = 16
        before = max(e.offset - head, 0)
        show = buf.text[before:e.offset + tail]
        cursor_fmt = "%%+%ds" % (e.offset - before + 1)

        print show
        print cursor_fmt % "^"

    if result[0]:
        print "Syntax OK."
    else:
        for e in result[1]:
            perror(e, buf)


class RulesTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(RulesTest, self).__init__(*args, **kwargs)

        self.bnf = BNF(no_cache=True)

    #@unittest.SkipTest
    def test_shared_start(self):
        self.assertEqual(shared_start('abc', 'abd'), 'ab')

    #@unittest.SkipTest
    def test_string_ok(self):
        L = String(self.bnf, 'language')
        node = L.parse('language')

        self.assertEqual(node.span, (0, len('language')))

    #@unittest.SkipTest
    def test_string_nok(self):
        L = String(self.bnf, 'language')
        self.assertRaises(ParsingError, L.parse, 'Language')

    #@unittest.SkipTest
    def test_chain_ok(self):
        R = String(self.bnf, 'russian')
        B = String(self.bnf, ' ')
        I = String(self.bnf, 'italian')
        L = Chain(self.bnf, (R, B, I))

        node = L.parse('russian italian')
        russian, blank, italian = node.children

        self.assertEqual(russian.text, 'russian')
        self.assertEqual(blank.text, ' ')
        self.assertEqual(italian.text, 'italian')


    #@unittest.SkipTest
    def test_string_w_tail(self):
        T = String(self.bnf, 'text')

        self.assertRaises(ParsingError, T.parse, 'text ', align='fit')
        node = T.parse('text ', align='left')
        self.assertEqual((0, 4), node.span)

    #@unittest.SkipTest
    def test_re(self):
        A = Re(None, 'a+')
        node = A.parse('aaaa', align='fit')
        self.assertEqual(node.span, (0, 4))

        node = A.parse('aa$$$', align='left')
        self.assertEqual(node.span, (0, 2))

        self.assertRaises(ParsingError, A.parse, '^aaa')

    #@unittest.SkipTest
    def test_repeat(self):
        R = Repeat(self.bnf, String(None, 'a'), 'R', 2, 3)

        self.assertRaises(ParsingError, R.parse, 'a')

        node = R.parse('aa')
        self.assertEqual(node.span, (0, 2))

        node = R.parse('aaa')
        self.assertEqual(node.span, (0, 3))

        self.assertRaises(ParsingError, R.parse, 'aa_', align='fit')

        node = R.parse('aa_', align='left')
        self.assertEqual(len(node.children), 2)
        self.assertEqual(node.span, (0, 2))

    #@unittest.SkipTest
    def test_repeat_no_range(self):
        R = Repeat(self.bnf, String(None, 'a'), 'R', 2, None)

        R.parse('aa')
        self.assertRaises(ParsingError, R.parse, 'a')
        self.assertRaises(ParsingError, R.parse, 'aaa')

    #@unittest.SkipTest
    def test_repeat_greedy(self):
        R = Repeat(self.bnf, String(None, 'a'), 'R', 2, 0)

        R.parse('aa')
        text = 'aaaaaaaaaaaaa'
        node = R.parse(text)
        self.assertEqual(len(node.children), len(text))

    #@unittest.SkipTest
    def test_repeat_any(self):
        #import sys

        R = Repeat(self.bnf, String(None, 'a'), 'R', 0, 0)

        off = 10
        for text in ('', 'a', 'aa'):
            node = R.parse(text, offset=off)
            #node.xml_tree().write(sys.stdout)
            self.assertEqual(node.span, (off, off + len(text)))

    def test_products(self):
        p1 = Product(terminal_level=0)
        p2 = Product(terminal_level=2)
        p2.looped = True

        class FakeRule(object):
            def __init__(self):
                self.product = None

            #def __cmp__(self, other):
            #    return cmp(self.product, other.product)

            def __repr__(self):
                return repr(self.product)

        r1 = FakeRule()
        r1.product = p1
        r2 = FakeRule()
        r2.product = p2

        print cmp(r1.product, r2.product)
        print sorted([r1, r2])
        print sorted([r1, r2], key=lambda r: r.product)

if __name__ == "__main__":
    unittest.main()
