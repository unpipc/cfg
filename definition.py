#!/usr/bin/env python

from rules import *
from rules import String
import unittest
import abc
from errors import *

class Definition(object):
    @classmethod
    def to_definition(cls, record):
        if isinstance(record, Definition):
            return record

        compound_types = (list, tuple, set, frozenset)

        # type(record): (<Devinition class>, <flatten arguments?>)
        string_type = (STR, False)
        list_type = (CHAIN, True)
        set_type = (CHOICE, True)

        types_map = {
            str: string_type,
            unicode: string_type,
            list: list_type,
            tuple: list_type,
            set: set_type,
            frozenset: set_type,
        }

        bnf_assert(isinstance(record, tuple(types_map.keys())), "Bad BNF record: %s" % repr(record))

        if isinstance(record, compound_types):
            bnf_assert(len(record), "Empty definition.")

            if len(record) == 1:
                return cls.to_definition(list(record)[0])

        definition_class, flatten = types_map[type(record)]
        return definition_class(*(record if flatten else (record,)))

    def get_rule(self, bnf, symbol=None):
        raise Exception('Not implemented')


class LAMBDA_DEF(Definition):
    def get_rule(self, bnf, symbol=None):
        return Lambda(bnf, symbol)

LAMBDA = LAMBDA_DEF()

class STR(Definition):
    """ Simple chain of characters. """

    def __init__(self, string):
        bnf_assert(isinstance(string, basestring), "'string' must be of type basestring")
        self.string = string

    def __repr__(self):
        return repr(self.string)

    def get_rule(self, bnf, symbol=None):
        return String(bnf, self.string, symbol)


class RE(Definition):
    """ Regular expression. """

    def __init__(self, rexp):
        self.rexp = rexp

    def __repr__(self):
        return 'RE:%s' % repr(self.rexp)

    def get_rule(self, bnf, symbol=None):
        return Re(bnf, self.rexp, symbol)


class SYM(Definition):
    """ Reference to a definition via its symbol. """

    def __init__(self, symbol):
        self.ref = symbol

    def __repr__(self):
        return self.ref

    def get_rule(self, bnf, symbol=None):
        return Ref(bnf, self.ref)


class CompoundDefinition(Definition):
    __metaclass__ = abc.ABCMeta

    def __init__(self, *items):
        self.items = map(self.to_definition, items)

    def name(self):
        return '<Compound definition>'


class CHAIN(CompoundDefinition):
    def name(self):
        return 'CHAIN'

    def __repr__(self):
        return '(%s)' % ' '.join(map(str, self.items))

    def get_rule(self, bnf, symbol=None):
        return Chain(bnf, [item.get_rule(bnf) for item in self.items], symbol)


class CHOICE(CompoundDefinition):
    def name(self):
        return 'CHOICE'

    def __repr__(self):
        return '[%s]' % ' | '.join(map(str, self.items))

    def get_rule(self, bnf, symbol=None):
        return Choice(bnf, [item.get_rule(bnf) for item in self.items], symbol)


class REPEAT(Definition):
    def __init__(self, record, nmin, nmax=None):
        bnf_assert(nmin >= 0, "nmin must be >=0.")
        bnf_assert(nmax is None or nmax == 0 or nmin <= nmax, "nmin must be <= nmax")

        self.what = self.to_definition(record)
        self.nmin = nmin
        self.nmax = nmax

    def __repr__(self):
        if self.nmin == self.nmax:
            nums = "%d" % self.nmin
        else:
            nums = "%d-%s" % (self.nmin, '' if self.nmax is None else str(self.nmax))

        return '%s%s' % (self.what, '' if nums == '1' else '*%s' % nums)

    def get_rule(self, bnf, symbol=None):
        return Repeat(bnf, self.what.get_rule(bnf), symbol, self.nmin, self.nmax)


class OPT(REPEAT):
    def __init__(self, record):
        super(OPT, self).__init__(record, 0, 1)


class ANY(REPEAT):
    def __init__(self, record):
        super(ANY, self).__init__(record, 0, 0)


class Test(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test, self).__init__(*args, **kwargs)
        self.cast = Definition.to_definition

    def test_definitions(self):
        STR('poor fantasy')

        RE('[a-z]{1, 3}')
        SYM('A')

        REPEAT(SYM('A'), 1, 2)
        REPEAT(SYM('A'), 1)
        self.assertRaises(BnfError, REPEAT, SYM('A'), 3, 1)

        numbers = map(str, range(10))
        CHAIN(*numbers)
        CHOICE(*numbers)

        self.assertRaises(BnfError, CHAIN, '0', 1)

    def test_casting(self):
        self.cast({
            'a',
            REPEAT(SYM('A'), 1, 1),
            CHAIN(
                RE('0-9'),
                RE('a-z'),
                STR('str'),
                {
                    REPEAT(SYM('A'), 2, 2),
                    'alt'
                }
            )
        })

        self.assertEqual(type(self.cast(['aaa', 'bbb'])), CHAIN)
        self.assertEqual(type(self.cast(('aaa', 'bbb'))), CHAIN)

        # Not CHAIN, but STR because CHAIN would contain only one
        # element which is awkward.
        self.assertEqual(type(self.cast(['single_element'])), STR)

        self.assertRaises(BnfError, self.cast, object)

    def test_empty_definitions(self):
        self.assertRaises(BnfError, self.cast, [])
        self.assertRaises(BnfError, self.cast, ())
        self.assertRaises(BnfError, self.cast, {})


if __name__ == "__main__":
    unittest.main()
