#!/usr/bin/env python

import unittest
from itertools import takewhile
from debug import Debug, debug


def group(items, counters):
    items = iter(items)

    for c in counters:
        y = []

        for i in xrange(c):
            y.append(next(items))

        yield tuple(y)


def squeeze(items, count=False, only_values=set()):
    items = iter(items)
    y = lambda what, how_many: (what, how_many) if count else what

    counter = 0

    for i, val in enumerate(items):
        if i == 0:
            counter += 1
        else:
            if val == prev_val and (not only_values or val in only_values):
                counter += 1
            else:
                yield y(prev_val, counter)
                counter = 1

        prev_val = val

    if counter:
        yield y(prev_val, counter)


class SpanError(Exception):
    def __init__(self, index, span_no):
        self.index = index
        self.span_no = span_no

    def __str__(self):
        s = "Span error at index %d" % self.index

        #return s if self.span_no is None else "%s, span in error: %d" % (s, self.span_no)
        return "%s, span in error: %d" % (s, self.span_no)

    def __repr__(self):
        return 'SpanError(%d, %d)' % (self.index, self.span_no)


def normalize(main_span, spans, mask):
    within_main = lambda span: span[0] >= main_span[0] and span[1] <= main_span[1]
    spans = filter(within_main, spans)

    chunk = lambda c1, c2: ((c1, c2), True)
    blank = lambda c1, c2: ((c1, c2), False)
    void_blank = lambda c: ((c, c), False)

    is_void = lambda span: span[0][0] == span[0][1]
    is_chunk = lambda x: x[1]
    is_blank = lambda x: not x[1]
    need_at = lambda pos: {0: 'blank', 1: 'chunk'}[int(mask[pos])] if pos < len(mask) else 'eof'
    is_eof = lambda cursor: main_span[1] == cursor

    def error(expected, at, to=None, mask_bit=None):
        return {'result': None,
                'error': {'expected': expected,
                          'at': at,
                          'to': to,
                          'mask_bit': mask_bit}}

    result = []

    text_cursor = main_span[0]
    span_no = None
    mask_cursor = 0

    for span_no, span in enumerate(spans):
        need = need_at(mask_cursor)
        b = blank(text_cursor, span[0])

        while need == 'blank':
            result.append(b)
            text_cursor = span[0]
            mask_cursor += 1
            need = need_at(mask_cursor)
            b = blank(text_cursor, span[0])

        if need == 'chunk' and not is_void(b):
            return error('chunk', text_cursor, None, mask_cursor)

        if need == 'eof':
            return error('eof', text_cursor)
        else:
            result.append(chunk(span[0], span[1]))

            text_cursor = span[1]
            mask_cursor += 1

    need = need_at(mask_cursor)
    b = blank(text_cursor, main_span[1])

    while need == 'blank':
        result.append(b)

        text_cursor = main_span[1]
        b = void_blank(text_cursor)

        mask_cursor += 1
        need = need_at(mask_cursor)

    if need == 'chunk':
        last = result[-1] if result else None

        if not last or is_chunk(last):
            at = last[0][1] if last else main_span[0]
            to = None
        else:
            at = last[0][0]
            to = main_span[1]

        return error('chunk',
                     at,
                     to,
                     mask_cursor)

    if need == 'eof' and not is_eof(text_cursor):
        return error('eof', text_cursor)

    return {'result': tuple(result)}


def distribute(spans_list, pos=None, greedy=True):
    spans_list = list(takewhile(lambda spans: bool(spans), spans_list))

    if not spans_list:
        yield []
        return

    if greedy:
        get_first = lambda items: items[-1]
        get_rest = lambda items: items[:-1] if len(items) > 1 else None
        good_position = lambda span: span[1] <= pos
        new_pos = lambda span: span[0]
        sort_key = lambda span: (-span[0], -span[1])
        concat = lambda distribution, span: distribution + [span]
    else:
        good_position = lambda span: span[0] >= pos
        new_pos = lambda span: span[1]
        get_first = lambda items: items[0]
        get_rest = lambda items: items[1:] if len(items) > 1 else None
        sort_key = lambda span: span
        concat = lambda distribution, span: [span] + distribution

    first = get_first(spans_list)
    rest = get_rest(spans_list)

    candidates = sorted(filter(good_position, first) if pos is not None else first, key=sort_key)

    if rest:
        if candidates:
            for c in candidates:
                for d in distribute(rest, new_pos(c), greedy=greedy):
                    if d:
                        yield concat(d, c)
        elif greedy:
            if rest:
                for d in distribute(rest, greedy=greedy):
                    yield d
    elif candidates:
        for span in candidates:
            yield [span]



def get_spans(text, rexp):
    for i in xrange(len(text) + 1):
        m = rexp.match(text[i:])

        if m:
            s = m.span()
            yield s[0] + i, s[1] + i


def distributions(text, rexps, mask, **kwargs):
    align = kwargs.get('align', 'fit')
    greedy = kwargs.get('greedy', False)
    mask = map(lambda bit: bool(int(bit)), mask)

    assert align in {'fit', 'left', 'right'}

    if align == 'left' and mask[-1]:
        mask.append(False)
        remove_padding = lambda items: items[:-1]
        adjust_mask_bit = lambda mask_bit: mask_bit
    elif align == 'right' and mask[0]:
        mask.insert(0, False)
        remove_padding = lambda items: items[1:]
        adjust_mask_bit = lambda mask_bit: mask_bit - 1
    else:
        remove_padding = lambda items: items
        adjust_mask_bit = lambda mask_bit: mask_bit

    errors = []
    yielded = False

    spans = [list(get_spans(text, rexp)) for rexp in rexps]
    debug("spans for %s: %s" % (text, repr(spans)), 3)

    for distribution in distribute(spans, greedy=greedy):
        norm = normalize((0, len(text)), distribution, mask)
        result = norm['result']

        if result is None:
            errors.append(norm['error'])
        else:
            yield {'result': remove_padding(norm['result'])}
            yielded = True

    if errors and not yielded:
        error = dict(sorted(errors, key=lambda e: -e['at'])[0])
        mask_bit = error['mask_bit']
        error['mask_bit'] = adjust_mask_bit(mask_bit) if mask_bit is not None else None

        yield {'result': None,
               'error': error}


class Test(unittest.TestCase):
    def distributions(self, *args, **kwargs):
        return list(distributions(*args, **kwargs))

    #@unittest.SkipTest
    def test_void(self):
        result = [d['result'] for d in self.distributions("text", [], [False])]
        self.assertEqual(result, [(((0, 4), False),)])

    #@unittest.SkipTest
    def test_distribute_1(self):
        self.assertEqual(list(distribute([[], [(0, 1)]])), [[]])
        self.assertEqual(list(distribute([[(0, 1)], [], [(3, 5)]])), [[(0, 1)]])

        spans = [[(0, 1), (17, 18), (42, 43)], [(0, 5), (17, 24), (42, 49)]]
        expected = [[(17, 18), (42, 49)], [(0, 1), (42, 49)], [(0, 1), (17, 24)]]
        self.assertEqual(list(distribute(spans)), expected)

    #@unittest.SkipTest
    def test_normalize_1(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "abab"

        expect = [( ((0, 0), False), ((0, 1), True), ((1, 1), False), ((1, 2), True), ((2, 4), False) ),
                  ( ((0, 0), False), ((0, 1), True), ((1, 3), False), ((3, 4), True), ((4, 4), False) ),
                  ( ((0, 2), False), ((2, 3), True), ((3, 3), False), ((3, 4), True), ((4, 4), False) )]

        result = [d['result'] for d in self.distributions(text, [a, b], [False, True, False, True, False])]
        self.assertEqual(result, expect)

    #@unittest.SkipTest
    def test_normalize_misses(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "abab"

        expect = [( ((0, 1), True), ((1, 3), False), ((3, 4), True) )]
        result = [d['result'] for d in self.distributions(text, [a, b], [True, False, True])]
        self.assertEqual(result, expect)

    #@unittest.SkipTest
    def test_aligning(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "abab"

        expect = [(((0, 1), True), ((1, 2), True))]
        result = [d['result'] for d in self.distributions(text, [a, b], "11", align='left')]
        self.assertEqual(result, expect)

        expect = [(((2, 3), True), ((3, 4), True))]
        result = [d['result'] for d in self.distributions(text, [a, b], "11", align='right')]
        self.assertEqual(result, expect)

    #@unittest.SkipTest
    def test_greed_1(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "abab"

        expect = [(((0, 2), False), ((2, 3), True), ((3, 3), False), ((3, 4), True), ((4, 4), False)),
                  (((0, 0), False), ((0, 1), True), ((1, 3), False), ((3, 4), True), ((4, 4), False)),
                  (((0, 0), False), ((0, 1), True), ((1, 1), False), ((1, 2), True), ((2, 4), False))]
        result = [d['result'] for d in self.distributions(text,
                                                          [a, b],
                                                          [False, True, False, True, False],
                                                          greedy=True)]
        self.assertEqual(result, expect)

    def test_squeeze(self):
        self.assertEqual(list(squeeze([])), [])
        self.assertEqual(list(squeeze([], count=True)), [])

        self.assertEqual(list(squeeze([0])), [0])
        self.assertEqual(list(squeeze([0], count=True)), [(0, 1)])

        self.assertEqual(list(squeeze([0, 0, 1, None, None, None, False, False])),
                         [0, 1, None, False])
        self.assertEqual(list(squeeze([0, 0, 1, None, None, None, False, False], count=True)),
                         [(0, 2), (1, 1), (None, 3), (False, 2)])

    def test_squeeze_selected_void(self):
        self.assertEqual(list(squeeze([1], only_values={0}, count=True)),
                         [(1, 1)])

    def test_squeeze_selected(self):
        self.assertEqual(list(squeeze([0, 0, 1, 1, 2, 2, 2], only_values={0, 2}, count=True)),
                         [(0, 2), (1, 1), (1, 1), (2, 3)])

    def test_group(self):
        self.assertEqual(list(group([], ())),
                         [])
        self.assertEqual(list(group([1], (1,))),
                         [(1,)])
        self.assertEqual(list(group([1, 1, 2, 2, 2, 3], (2, 3, 1))),
                         [(1, 1), (2, 2, 2), (3,)])

    #@unittest.SkipTest
    def test_error_left(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "a"

        self.assertEqual(self.distributions(text, [a, b], "11", align='left')[0]['error'],
                         {'expected': 'chunk',
                          'at': 1,
                          'to': None,
                          'mask_bit': 1})

    #@unittest.SkipTest
    def test_error_0_0_case(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "b"

        self.assertEqual(self.distributions(text, [a, b], "11", align='left')[0]['error'],
                         {'expected': 'chunk',
                          'at': 0,
                          'to': None,
                          'mask_bit': 0})

    #@unittest.SkipTest
    def test_error_fit(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "ab_"

        error = self.distributions(text, [a, b], "11", align='fit')[0]['error']
        self.assertEqual(error, {'expected': 'eof',
                                 'at': 2,
                                 'to': None,
                                 'mask_bit': None})

    #@unittest.SkipTest
    def test_error_short_distribution(self):
        a = re.compile('a')
        b = re.compile('b')
        text = "a"

        error = self.distributions(text, [a, b], "11", align='fit')[0]['error']
        self.assertEqual(error, {'expected': 'chunk',
                                 'at': 1,
                                 'to': None,
                                 'mask_bit': 1})

    #@unittest.SkipTest
    def test_error_index(self):
        a = re.compile('a')
        b = re.compile('b+')
        text = "ab_b"

        greedy = True
        error = self.distributions(text, [a, b], "11", align='fit', greedy=greedy)[0]['error']
        self.assertEqual(error, {'expected': 'eof',
                                 'at': 2,
                                 'to': None,
                                 'mask_bit': None})

    #@unittest.SkipTest
    def test_normalize_no_spans(self):
        # We do not have any spans and not require any chunks either.
        self.assertEqual(normalize((10, 20), [], '0')['result'],
                         (((10, 20), False),))

        # No spans, but require a chunk.
        self.assertEqual(normalize((10, 20), [], '1')['error'],
                         {'expected': 'chunk',
                          'at': 10,
                          'to': None,
                          'mask_bit': 0})

        # No spans, but require a chunk after blank.
        self.assertEqual(normalize((10, 20), [], '01')['error'],
                         {'expected': 'chunk',
                          'at': 10,
                          'to': 20,
                          'mask_bit': 1})

        self.assertEqual(normalize((10, 20), [], '')['error'],
                         {'expected': 'eof',
                          'at': 10,
                          'to': None,
                          'mask_bit': None})

        self.assertEqual(normalize((10, 10), [(10, 11)], '1')['error'],
                         {'expected': 'chunk',
                          'at': 10,
                          'to': None,
                          'mask_bit': 0})


if __name__ == "__main__":
    import re
    unittest.main()
