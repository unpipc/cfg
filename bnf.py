#!/usr/bin/env python

from errors import *
from cache import Cache
from nodes import *
import sys
from collections import namedtuple
import unittest
import debug as dbg
from utils import _repr, _cut, _rule_at

RuleAt = namedtuple('RuleAt', ['rule', 'span'])


class BNF(object):
    def __init__(self, no_cache=False, thorough_parsing=True, auto_choice=True,
                 debug=0):
        self.definitions = dict()
        self.rules = dict()
        self._calling_stack = list()
        self.target_symbol = None
        self.deepest_error = None

        self.no_cache = no_cache
        self.thorough_parsing = thorough_parsing
        self.auto_choice = auto_choice

        dbg.Debug = int(debug)

        self.cache = Cache()
        self.__initialize()

    def __initialize(self):
        self.deepest_error = None
        self._callees = dict()
        self._looped = set()
        self._resolving_stack = []
        self._resolving_second_pass = False

    def define(self, symbol, record, target=False):
        """
        :param symbol:      Name of a symbol to be defined (left part in BNF).
        :param record:      Right part of the BNF.
        :return:            None

        Examples of usage:

        symbol          record                  Assumed BNF

        'A'             'abc',                  A ::= abc
        'A1'            STR('abc')              A1 :: = abc
        'R'             RE('abc[0-9]{1, 2}')    Simple regular expression, no BNF.
        'B'             SYM('A')                B ::= A
        'C'             [SYM('A'), 'defg']      C ::= Adefg

        'D'             {'foo', 'bar'}          D ::= foo
                                                D ::= bar

        'E'             MULT(SYM('A'), 1, 3)    E ::= A
                                                E ::= AA
                                                E ::= AAA

        'E'             MULT('abc', 1, 3)       E ::= abc
                                                E ::= abcabc
                                                E ::= abcabcabc

        'CMPLX'         (MULT(SYM('CMPLX'), 1, 3), 'def', {'ghi', 'jkl'})

        """

        from definition import Definition
        d = Definition.to_definition(record)

        self.definitions[symbol] = self.definitions.get(symbol, []) + [d]

        if target:
            bnf_assert(self.target_symbol in {None, symbol}, 'Target already specified')

            self.target_symbol = symbol

        from definition import SYM
        return SYM(symbol)

    def _before_parsing(self, span, rule):
        callee = RuleAt(rule=rule, span=span)
        caller = self._calling_stack[-1] if self._calling_stack else None
        callees = self._callees.get(caller, set())

        if callee in callees:
            dbg.debug("%s already called by %s" % (callee, caller), 1)
            return False
        else:
            dbg.debug("%s calls %s" % (_rule_at(caller), _rule_at(callee)), 1)

            self._callees[caller] = callees | {callee}
            self._calling_stack.append(callee)

            return True

    def _after_parsing(self):
        callee = self._calling_stack.pop()
        caller = self._calling_stack[-1] if self._calling_stack else None

        self._callees[caller] -= {callee}

    def parse(self, text, align='fit'):
        from definition import Definition, CHOICE
        self.__initialize()

        bnf_assert(self.target_symbol is not None, 'Target symbol not specified')

        to_definition = lambda pieces: Definition.to_definition(pieces[0] if len(pieces) == 1 else CHOICE(*pieces))

        definitions = {symbol: to_definition(pieces) for symbol, pieces in self.definitions.iteritems()}
        self.rules = {symbol: d.get_rule(self, symbol) for symbol, d in definitions.iteritems()}

        target = self.rules[self.target_symbol]
        self._resolve_rule(target)

        dbg.set_margin_func(lambda: '  ' * len(self._calling_stack))

        try:
            return self._parse_rule(target, text, 0, align)
        except ParsingError as e:
            raise self.deepest_error

    def _parse_rule(self, rule, text, offset, align):
        dbg.debug("%s:%s on `%s'" % (rule, align, _cut(text)))

        text_span = (offset, offset + len(text))

        if not self._before_parsing(text_span, rule):
            raise LoopingError(rule, text_span, align)

        try:
            if not self.no_cache:
                x = self.cache.get(rule, text_span, align)

                if isinstance(x, Exception):
                    raise x
                elif isinstance(x, NodeBase):
                    return x

            node = rule.parse(text, offset, align)
            self.cache.set_node(node, rule)
            dbg.debug("!%s:%s%s on `%s'" % (rule, align, node.span, _cut(text)))
        except ParsingError, e:
            self.cache.set_error(e, rule, text_span, align)

            if self.deepest_error is None or e.index > self.deepest_error.index:
                self.deepest_error = e

            raise
        finally:
            self._after_parsing()

        return node

    def _resolve_rule(self, rule):
        self.resolve(rule)
        self._resolving_second_pass = True
        product = self.resolve(rule)

        for r in self.rules.itervalues():
            bnf_assert(r.product,
                       "Non-productive or unreachable rule %s" % r)

        for r in self._looped:
            r.product.looped = True
            dbg.debug("Looped product for: %s" % r, 10)

        return product

    def resolve(self, rule):
        if rule.product and not self._resolving_second_pass:
            return rule.product

        try:
            short_cut_at = self._resolving_stack.index(rule)
            dbg.debug('Shortcut: %s on stack %s' % (rule, self._resolving_stack), 10)

            new_looped = set(self._resolving_stack[short_cut_at:])
            dbg.debug("Looping: %s" % _repr(new_looped), 10)

            self._looped |= new_looped

            return rule.product
        except ValueError:
            pass

        self._resolving_stack.append(rule)
        product = rule.resolve()
        self._resolving_stack.pop()

        return product

    def _error_preference_key(self):
        def key_func(e):
            key = (-e.index, e.rule.product if e.rule else None)
            dbg.debug("error key: {}".format(key))

            return key

        return key_func

class BnfTest(unittest.TestCase):
    #@unittest.SkipTest
    def test_non_productive_self_reference(self):
        bnf = BNF()
        bnf.define('A', SYM('A'), target=True)
        self.assertRaises(BnfError, bnf.parse, '')

    #@unittest.SkipTest
    def test_non_productive_self_reference_chain(self):
        bnf = BNF()
        bnf.define('A', ['aaa', SYM('A')], target=True)
        self.assertRaises(BnfError, bnf.parse, '')

    #@unittest.SkipTest
    def test_productive_self_reference_deep(self):
        bnf = BNF()
        bnf.define('A', 'aaa')
        bnf.define('B', {(SYM('A'), 'bbb'), SYM('B')}, target=True)
        bnf.parse('aaabbb')
        self.assertEqual(bnf.rules['B'].product.terminal_level, 3)

    #@unittest.SkipTest
    def test_basic(self):
        bnf = BNF()
        bnf.define('A', 'aaa')
        bnf.define('B', 'bbb')
        bnf.define('S', [SYM('A'), SYM('B')], target=True)

        bnf.parse('aaabbb')
        self.assertEqual(bnf.rules['S'].product.terminal_level, 2)

    #@unittest.SkipTest
    def test_parsing_trivial(self):
        bnf = BNF()
        bnf.define('A', 'aaa', target=True)
        bnf.parse('aaa')

    #@unittest.SkipTest
    def test_parsing_basic_chain(self):
        bnf = BNF()
        bnf.define('A', ('aaa', 'bbb'), target=True)

        bnf.parse('aaabbb')

    #@unittest.SkipTest
    def test_parsing_basic_chain_ref(self):
        bnf = BNF()
        bnf.define('A', 'aaa')
        bnf.define('B', 'bbb')
        bnf.define('S', (SYM('A'), SYM('B')), target=True)

        node = bnf.parse('aaabbb')
        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_parsing_basic_choice(self):
        bnf = BNF()
        bnf.define('A', {'aaa', 'bbb'}, target=True)

        for text in ['aaa', 'bbb']:
            node = bnf.parse('aaa')
            #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_parsing_basic_choice_chain(self):
        bnf = BNF()
        bnf.define('A', [{'aaa', 'bbb'}, 'CCC'], target=True)

        for text in ('aaaCCC', 'bbbCCC'):
            bnf.parse(text)

    #@unittest.SkipTest
    def test_syntax_error_string(self):
        bnf = BNF()
        bnf.define('A', 'aaa', target=True)

        self.assertRaises(ParsingError, bnf.parse, 'aa')

    #@unittest.SkipTest
    def test_syntax_error_chain(self):
        bnf = BNF()
        bnf.define('A', ['aaa', 'bb'], target=True)

        bnf.parse('aaabb')
        self.assertRaises(ParsingError, bnf.parse, 'aaab')

    #@unittest.SkipTest
    def test_parsing_basic_choice_chain_complex(self):
        bnf = BNF()
        bnf.define('A', [{'aaa', 'bbb'}, 'DDD'])
        bnf.define('B', [{'aaa', 'ccc'}, 'DDD'])
        bnf.define('S', SYM('A'), target=True)
        bnf.define('S', SYM('B'))

        for text in ('aaaDDD', 'bbbDDD', 'cccDDD'):
            bnf.parse(text)

        self.assertRaises(ParsingError, bnf.parse, 'aaa')


    #@unittest.SkipTest
    def test_optional(self):
        bnf = BNF()
        bnf.define('S', OPT(SYM('S')), target=True)
        node = bnf.parse('')
        self.assertEqual(node.span, (0, 0))

    #@unittest.SkipTest
    def test_arithmetic_language(self):
        bnf = BNF()
        bnf.define('Op', {'-', '+', '*', '/'})
        bnf.define('Num', RE('[0-9]+'))

        bnf.define('Exp', SYM('Num'), target=True)
        bnf.define('Exp', [SYM('Exp'), SYM('Op'), SYM('Exp')])
        bnf.define('Exp', ['(', SYM('Exp'), ')'])

        self.assertRaises(ParsingError, bnf.parse, '')
        node = bnf.parse('(0)')
        node = bnf.parse('12-(3*8-24/2)')
        self.assertRaises(ParsingError, bnf.parse, '0+(1+2')

        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_left_recursion(self):
        bnf = BNF(thorough_parsing=False )
        bnf.define('A', [SYM('A'), 'a'], target=True)
        bnf.define('A', 'a')

        node = bnf.parse('aaa')
        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_right_recursion(self):
        bnf = BNF()
        bnf.define('B', ['b', SYM('B')], target=True)
        bnf.define('B', 'b')

        node = bnf.parse('bbb')
        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_infinite_loops(self):
        bnf = BNF()
        bnf.define('D', SYM('D'))
        bnf.define('L', [SYM('L'), SYM('D')], target=True)
        bnf.define('L', 'l')

        self.assertRaises(BnfError, bnf.parse, 'l')

    #@unittest.SkipTest
    def test_greed(self):
        bnf = BNF(thorough_parsing=False)
        bnf.define('G', [SYM('G'), 'g'], target=True)
        bnf.define('G', 'g')

        node = bnf.parse('ggg', align='left')
        #node.xml_tree().write(open('/tmp/out', 'w'))
        self.assertEqual(node.span, (0, 3))

    #@unittest.SkipTest
    def test_greed_non_terminal(self):
        bnf = BNF()
        bnf.define('Op', {'-', '+', '*', '/'})
        bnf.define('Num', RE('[0-9]+'))

        bnf.define('Exp', SYM('Num'), target=True)
        bnf.define('Exp', [SYM('Exp'), SYM('Op'), SYM('Exp')])
        bnf.define('Exp', ['(', SYM('Exp'), ')'])

        node = bnf.parse('1+(2-3)_', align='left')
        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_brackets_language_greedy(self):
        bnf = BNF()
        bnf.define('S', ANY(SYM('S')), target=True)
        bnf.define('S', ['(', SYM('S'), ')'])
        bnf.define('S', ['[', SYM('S'), ']'])
        bnf.define('S', LAMBDA)

        text = '([()()])([[()()()]])'
        node = bnf.parse(text + 'tail', 'left')
        #node.xml_tree().write(sys.stdout)

        self.assertEqual(node.span, (0, len(text)))

    #@unittest.SkipTest
    def test_prog_func_call(self):
        bnf = BNF(debug=6)
        b = bnf.define('Blank', RE('[\n\t ]*'))
        op = bnf.define('Op', RE('[-+*/%]'))
        num = bnf.define('Num', RE('[0-9]+'))
        exp = bnf.define('Exp', {num,
                                 (SYM('Exp'), op, SYM('Exp')),
                                 ('(', SYM('Exp'), ')')})

        name = bnf.define('Name', RE('[_a-zA-Z]+'))
        arg_list = bnf.define('Arg_list', {LAMBDA,
                                           (b, SYM('Exp'), ANY([b, ',', b, SYM('Exp'), b]))})
        fcall = bnf.define('Fcall', [name, '(', arg_list, ')'], target=True)

        text = 'func(0,  1, 2+(3-4))'
        node = bnf.parse(text)
        #node.xml_tree().write(sys.stdout)

    #@unittest.SkipTest
    def test_error_reporting(self):
        bnf = BNF()

        bnf.define('B', ['aaa ', 'bbb '])
        bnf.define('C', {'ccc', 'ddd'})

        bnf.define('A', [SYM('B'), 'tail1'])
        bnf.define('A', ['head2 ', SYM('C')])

        bnf.define('P', ANY([SYM('A'), '\n']), target=True)

        text = 'aaa bbb tail1\naaa bbb tail2\n'

        try:
            node = bnf.parse(text)
            node.xml_tree().write(sys.stdout)
        except ParsingError as e:
            einfo = error_info(e, text, surrounding_patterns=('...', '...'), surrounding_chars=(10000, 1000))

            self.assertFalse(einfo['eof_error'])
            self.assertEqual(einfo['error_line_info']['error_pointer'], 12)


if __name__ == "__main__":
    from definition import LAMBDA, SYM, RE, REPEAT, OPT, ANY
    unittest.main()
