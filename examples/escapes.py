#!/usr/bin/env python
from cfg import *
import sys

bnf = BNF()
bnf.define('B', ['[', SYM('B'), ']'], target=True)
bnf.define('B', ['(', SYM('B'), ')'])
bnf.define('B', RE('[a-z]*'))

node = bnf.parse('([(abc)])')
node.xml_tree().write(sys.stdout)
