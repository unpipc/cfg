#!/usr/bin/env python
from cfg import *
import sys

c = BNF()

_ = c.define('blank', RE('[\t\n ]*'))
ctype = c.define('ctype', RE('void|char|short|int|long|float|double'))
id = c.define('id', RE('[_a-zA-Z]([_a-zA-Z0-9])*'))
type_list = c.define('type_list', [ctype, _, ANY([',', _, ctype])])
eos = c.define('eos', RE('[\t\n ]*;[\t\n ]*'))
statement = c.define('statement', [{LAMBDA,
                                    SYM('exp'),
                                    SYM('fcall')}, eos])
block = c.define('block', ['{', _, ANY(statement), _, '}'])
fdef = c.define('fdef', [_, ctype, _, id, _, '(', _, type_list, _, ')', _, block], target=True)

op = c.define('op', RE('[-+*/]'))
num = c.define('num', RE('[0-9]+(\.[0-9]*)?'))
exp = c.define('exp', {num,
                       SYM('fcall'),
                       (SYM('exp'), _, op, _, SYM('exp')),
                       ('(', _, SYM('exp'), _, ')')})
arg_list = c.define('arg_list', {LAMBDA,
                                 (exp, _, ANY([',', _, exp]))})
fcall = c.define('fcall', [id, _, '(', _, arg_list, _, ')'])

with open('clang.c') as cfile:
    cprog = cfile.read()

    try:
        c.parse(cprog).xml_tree().write(sys.stdout)
    except ParsingError as e:
        einfo = error_info(e,
                           cprog,
                           surrounding_patterns=('...', '...'),
                           surrounding_chars=(32, 8))

        if einfo['eof_error']:
            print 'Unexpected end of file.'
        else:
            print einfo['error_line_info']['formatted_w_pointer']
