#!/usr/bin/env python
from cfg import *
import sys

bnf = BNF(auto_choice=False)

mult_op = bnf.define('MULT_OP', RE('[/*]'))
num = bnf.define('Num', RE('[0-9]+(\.[0-9]*)?'))
var = bnf.define('VAR', RE('[a-zA-Z_.][a-zA-Z_.0-9]*'))

g = bnf.define('G', ['(', SYM('EXP'), ')'])
p1 = bnf.define('P1', CHOICE(SYM('PROD'), g, num, var))
prod = bnf.define('PROD', [p1, mult_op, p1])
add = bnf.define('ADD', [SYM('EXP'), '+', SYM('EXP')])
sub = bnf.define('SUB', [SYM('EXP'), '-', p1])
bnf.define('EXP', g)
bnf.define('EXP', add)
bnf.define('EXP', sub)
bnf.define('EXP', prod)
bnf.define('EXP', num)
bnf.define('EXP', var)

eval = bnf.define('EVAL', ['$(', SYM('EXP'), ')'], target=True)

print
bnf.parse('$(1*2-3*(4+5)+6/7)').xml_tree().write(sys.stdout)
bnf.parse('$(1+(2+3))').xml_tree().write(sys.stdout)
bnf.parse('$(1+(2+3))').xml_tree().write(sys.stdout)
bnf.parse('$(a+b+3)').xml_tree().write(sys.stdout)
print
