#!/usr/bin/env python
from cfg import *
import sys

AB = BNF()
AB.define('T', [SYM('A'), SYM('B')], target=True)

AB.define('A', [SYM('A'), 'a'])
AB.define('A', 'a')

AB.define('B', [SYM('B'), 'b'])
AB.define('B', 'b')

P = BNF(thorough_parsing=False)
P.define('B', 'bbb ')
P.define('A', [SYM('B'), 'aaa '])
P.define('P', [SYM('A'), '$ '], target=True)

bnf = AB
text = 'aaabbbb'

#bnf = P
#text = 'bbb aaa $_'

try:
    node = bnf.parse(text).xml_tree().write(sys.stdout)
except ParsingError as e:
    einfo = error_info(e,
                       text,
                       surrounding_patterns=('...', '...'),
                       surrounding_chars=(32, 8))

    print e
    if einfo['eof_error']:
        print 'Unexpected end of file.'
    else:
        print einfo['error_line_info']['formatted_w_pointer']

