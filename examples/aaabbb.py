#!/usr/bin/env python
from cfg import *
import sys

bnf = BNF()
bnf.define('A', ['a', SYM('A'), 'b'], target=True)
bnf.define('A', LAMBDA)

try:
    text = 'aa@bb'
    node = bnf.parse(text)
    node.xml_tree().write(sys.stdout)
except ParsingError as e:
        einfo = error_info(e,
                           text,
                           surrounding_patterns=('...', '...'),
                           surrounding_chars=(32, 8))

        if einfo['eof_error']:
            print 'Unexpected end of file.'
        else:
            print einfo['error_line_info']['formatted_w_pointer']
