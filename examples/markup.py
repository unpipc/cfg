#!/usr/bin/env python

import sys
from cfg import BNF, SYM, RE, ANY, LAMBDA, CHOICE, ParsingError, error_info

g = BNF()

nl = g.define('nl', '\n')
_ = g.define('blank', RE('[\t ]*'))
h = g.define('hash', '##')
id = g.define('id', RE('[_a-zA-Z][_a-zA-Z0-1]{0,63}'))
num = g.define('num', RE('[0-9]+'))

g.define('exp', ['(', _, SYM('exp'), _, ')'])
g.define('exp', [SYM('exp'), _, RE('[+-/]'), _, SYM('exp')])
g.define('exp', num)
exp = g.define('exp', id)

esc = g.define('esc', RE('\\.'))
unicode_text = g.define('unicode_text', RE('[^\\#\n{}]+'))
block = g.define('block', ['{', SYM('text'), '}'])


arg = g.define('arg', CHOICE(exp, block))

arg_list = g.define('arg_list', CHOICE(LAMBDA, [arg, _, ANY([',', _, arg])]))
cmd = g.define('cmd', ['#', id, '(', _, arg_list, _, ')'])

special = g.define('special', CHOICE(cmd, h, esc))
text_element = g.define('text_element', CHOICE(special, unicode_text, block))
text = g.define('text', ANY(text_element))

p = g.define('p', [text, nl])
article = g.define('article', ANY(p), target=True)

def error(msg):
    sys.stderr.write(msg + '\n')

in_text = open('markup_text.txt').read().decode('utf-8')

try:
    root = g.parse(in_text)
    root.xml_tree().write(sys.stdout)
except ParsingError as e:
    einfo = error_info(e,
                       in_text,
                       surrounding_patterns=('...', '...'),
                       surrounding_chars=(32, 8))

    if einfo['eof_error']:
        error('Unexpected end of file.')
    else:
        error(einfo['error_line_info']['formatted_w_pointer'])
