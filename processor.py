#!/usr/bin/env python

import operator
from lib import *
from errors import *
from debug import Debug, debug
from utils import _cut


def parse_w_binding(bnf, text, rules, offset, align):
    debug("parse_w_binding: %s, %s, %s" % (_cut(text), rules, align), 3)
    first = operator.itemgetter(0)
    second = operator.itemgetter(1)
    is_terminal = operator.attrgetter('terminal')

    terminal_rules = filter(is_terminal, rules)

    rexps = map(lambda rule: rule.get_rexp(), terminal_rules)

    # Get the map of spans corresponding to the rules.
    # Non-terminal rules will require span (at least void tuple (x, x)).
    # Terminal rules will require regular expression matching span.
    #
    # E. g. for rule ABcD (where A, B and D are non-terminal with c
    # being terminal symbols) have '010' map.
    # So, later on we'll force the system to give us three spans.
    # First span will be matched against AB - flag 0
    # Second span will be matched to c - flag 1
    # Third span will be matched to D - flag 0
    squeezed = list(squeeze(map(is_terminal, rules), only_values={0}, count=True))
    mask = map(first, squeezed)
    rules_grouped = list(group(rules, map(second, squeezed)))

    text_distribs = distributions(text, rexps, mask, align=align, raise_on_error=True, greedy=True)
    errors = []

    for x in text_distribs:
        distrib = x['result']

        if distrib is None:
            e = x['error']

            if e['expected'] == 'chunk':
                mask_bit = e['mask_bit']
                rule = None if mask_bit is None else rules_grouped[mask_bit][0]

                end_index = offset + e['to'] if e['to'] is not None else None
                raise ParsingError(offset + e['at'], end_index=end_index, rule=rule)

            if e['expected'] == 'eof':
                raise ParsingError(offset + e['at'])

        nodes = []

        for i, zipped in enumerate(zip(distrib, rules_grouped)):
            ((start, end), span_type), group_of_rules = zipped

            aligning = align if i + 1 == len(distrib) else 'fit'
            try:
                more_nodes = parse(bnf, text[start:end], group_of_rules, offset + start, align=aligning)
            except ParsingError as e:
                errors.append(e)
                debug("error in distrib: %s" % e)
                break

            nodes.extend(more_nodes)

        if len(nodes) == len(rules):
            return nodes

    if errors:
        raise sorted(errors, key=bnf._error_preference_key())[0]
    else:
        raise ParsingError(offset, end_index=offset + len(text))

def parse(bnf, text, rules, offset, align='fit'):
    debug("parse(): %s, %s, %s" % (text, rules, align), 3)
    nodes = []
    cursor = 0

    for align, rule in zip(['left'] * (len(rules)-1) + [align], rules):
        debug("parse(), parsing %s, %s: %s" % (rule, align, text), 3)
        node = bnf._parse_rule(rule, text[cursor:], offset+cursor, align)
        nodes.append(node)
        cursor = node.span[1] - offset

    return nodes


class Test(unittest.TestCase):
    def test_parse_w_binding(self):
        pass

if __name__ == "__main__":
    unittest.main()
