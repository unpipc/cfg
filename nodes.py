#!/usr/bin/env python

# TODO: check if that works, too.
#import xml.etree.ElementTree as etree
from lxml import etree


class NodeBase(object):
    def __init__(self, tag, span):
        self.tag = tag
        self.span = span

        #FIXME: Very bulky. Uncomment on demand.
        #attrib = {'starts': str(span[0]),
        #          'ends': str(span[1])
        #}
        attrib = {'at': "%d-%d" % span}
        self._xml_element = etree.Element(self.tag, attrib)

    @property
    def xml_element(self):
        return self._xml_element

    def xml_tree(self):
        return etree.ElementTree(element=self.xml_element)

    def is_void(self):
        return self.span[0] == self.span[1]

    def __repr__(self):
        return "Node-%s@%d,%d" % (self.tag, self.span[0], self.span[1])


class NodeError(Exception):
    pass


class Node(NodeBase):
    def __init__(self, tag, *children, **kwargs):
        self.children = children

        if children:
            span = (children[0].span[0], children[-1].span[1])
        else:
            offset = kwargs.get('offset')
            span = (offset, offset)

        if span == (None, None):
            raise NodeError("Child nodes or 'offset' must be passed as arguments.")

        super(Node, self).__init__(tag, span)

    def xml_tree(self):
        if self.children:
            self.xml_element.extend(child.xml_tree().getroot() for child in self.children)

        return etree.ElementTree(element=self.xml_element)

class TextNode(NodeBase):
    def __init__(self, tag, text, offset):
        self.text = text

        super(TextNode, self).__init__(tag, (offset, offset + len(self.text)))

        self.xml_element.text = self.text


class LambdaNode(TextNode):
    def __init__(self, symbol, offset):
        super(LambdaNode, self).__init__("L-%s" % symbol, '', offset)

    def __repr__(self):
        return "LambdaNode-%s@%s" % (self.tag, self.span[0])