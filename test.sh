#!/bin/sh -

./bnf.py && ./cache.py && ./definition.py && ./errors.py && ./lib.py && ./processor.py && ./rules.py
result=$?

if [ $result -eq 0 ];
then
    echo "All tests passed."
else
    echo "Some or all tests failed."
fi

exit $result
