#!/usr/bin/env python

import re
import abc
import unittest


class LoopingError(Exception):
    def __init__(self, rule, span, align):
        super(LoopingError, self).__init__("Looping error for %s on span %s with aligning '%s'" % (rule, repr(span), align))
        self.rule = rule
        self.span = span
        self.align = align


class ParsingError(Exception):
    etype = 'syntax'

    def __init__(self, index, end_index=None, rule=None, message=None, reasons=[]):
        if message is not None:
            super(ParsingError, self).__init__(message)

        self.rule = rule
        self.index = index
        self.end_index = end_index
        self.message = message
        self.reasons = reasons

    def _set_info(self, error_info):
        self.info = error_info

    def __str__(self):
        def tabbed(text):
            return "\t" + "\n\t".join(text.split("\n"))

        if self.end_index is None:
            s = "Parsing error at %d." % self.index
        else:
            s = "Parsing error at interval (%d, %d)." % (self.index, self.end_index)

        if self.rule:
            s += " Expected %s." % self.rule

        if self.reasons:
            s = "\n".join([s, tabbed("\n".join([str(r) for r in self.reasons]))])

        return s


class ParsingCompleteError(Exception):
    pass


class BnfError(Exception):
    pass


def bnf_assert(cond, emsg):
    if not cond:
        raise BnfError(emsg)

class Test(unittest.TestCase):
    def test_node(self):
        i = ParsingError(500, rule="three")
        e = ParsingError(5, rule="two", reasons=[i])
        r = ParsingError(0, rule="one", reasons=[e])

        print r

    #@unittest.SkipTest
    def test_lines(self):
        self.assertEqual(list(lines('')), [])
        self.assertEqual(list(lines('1')), [(0, (0, 1), '1')])
        self.assertEqual(list(lines('1\n')), [(0, (0, 2), '1\n')])

        self.assertEqual(list(lines('1\n2')), [(0, (0, 2), '1\n'),
                                               (1, (2, 3), '2')])

    #@unittest.SkipTest
    def test_error_info(self):
        rule = 'Dummy rule object'
        error_info(ParsingError(0, rule=rule), '')
        self.assertRaises(Exception, error_info, ParsingError(1, rule), '')
        error_info(ParsingError(1, rule=rule), 'text')
        error_info(ParsingError(2, rule=rule), '1\n2\n')

        info = error_info(ParsingError(0, rule=rule), 'aaa bbb tail1\naaa bbb tail2\n')
        self.assertEqual(info['error_line_info']['error_pointer'], 0)

        self.assertEqual(error_info(ParsingError(12, rule=rule),
                                    '1\n|123456789e12345678|\n')['error_line_info']['surrounding_text'],
                         '|123456789e12345678|')

        self.assertEqual(error_info(ParsingError(13, rule=rule),
                                    '1\n-|123456789e012345678|-\n')['error_line_info']['surrounding_text'],
                         '...|123456789e012345678|...')

        self.assertEqual(error_info(ParsingError(1, rule=rule),
                                    '0123456789_0123456789')['error_line_info']['surrounding_text'], '0123456789_0...')

        self.assertEqual(error_info(ParsingError(14, rule=rule),
                                    '0123456789_0123456789')['error_line_info']['surrounding_text'], '...456789_0123456789')

        self.assertTrue(error_info(ParsingError(4, rule=rule), '1\n2\n').has_key('eof_error'))

        print error_info(ParsingError(26, rule=rule), 'aaa bbb tail1\naaa bbb tail2\n')


def lines(text):
    starts = 0
    line_no = None

    for line_no, nl in enumerate(re.compile('\n').finditer(text)):
        ends = nl.span()[1]
        yield (line_no, (starts, ends), text[starts:ends])
        starts = ends

    if text[starts:]:
        yield (0 if line_no is None else line_no + 1,
               (starts, len(text)),
               text[starts:])


def surrounding_text(text, at, surrounding_chars, surrounding_patterns):
    start = at - surrounding_chars[0]
    end = at + surrounding_chars[1] + 1

    prefix = surrounding_patterns[0] if start > 0 else ''

    if start <= 0:
        start = 0
        pointer = at
    else:
        pointer = len(surrounding_patterns[0]) + surrounding_chars[0]

    postfix = surrounding_patterns[1] if end < len(text) else ''

    return pointer, prefix + text[start:end] + postfix


def error_info(error, text, surrounding_chars=(10, 10), surrounding_patterns=('...', '...')):
    if error.index > len(text):
        raise Exception("Error is beyond the text")

    if surrounding_chars[0] < 0 or surrounding_chars[1] < 0:
        raise Exception("Bad surrounding chars.")

    eof_error = bool(error.index == len(text))
    error_line_info = None

    if not eof_error:
        for (line_no, (starts, ends), line) in lines(text):
            if error.index < ends:
                line_text = text[starts:ends]
                column_no = error.index - starts

                pointer, surr_text = surrounding_text(line_text.strip('\n'),
                                                      error.index - starts,
                                                      surrounding_chars=surrounding_chars,
                                                      surrounding_patterns=surrounding_patterns)

                if error.end_index:
                    annotation = "Line %d. %s error within interval starting at column %d: " % (line_no + 1,
                                                                                                error.etype.capitalize(),
                                                                                                column_no + 1)
                    pointer_char = '>'
                    pointer -= 1
                else:
                    annotation = "Line %d. %s error at column %d: " % (line_no + 1,
                                                                       error.etype.capitalize(),
                                                                       column_no + 1)
                    pointer_char = '^'

                nblanks = len(annotation) + pointer

                formatted_w_pointer = "%s%s\n%s%s" % (annotation,
                                                      surr_text,
                                                      ' ' * nblanks,
                                                      pointer_char)

                if error.message is not None:
                    formatted_w_pointer += "\n{blanks}{msg}".format(blanks=' ' * nblanks,
                                                                    msg=error.message)

                error_line_info = {'line_no': line_no,
                                   'column_no': column_no,
                                   'line_text': line_text,
                                   'surrounding_text': surr_text,
                                   'error_pointer': pointer,
                                   'formatted_w_pointer': formatted_w_pointer}

                break

    return {'eof_error': eof_error,
            'error_line_info': error_line_info}


if __name__ == "__main__":
    unittest.main()
